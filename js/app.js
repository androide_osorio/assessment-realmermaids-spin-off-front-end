// Foundation JavaScript
// Documentation can be found at: http://foundation.zurb.com/docs

(function(window, document, undefined) {

	/**
	 * document.ready
	 */
	window.jQuery(function($) {
		$(document).foundation();

		//initialize isotope
		var $grid = $('.isotope').isotope({
		  // set itemSelector so .grid-sizer is not used in layout
		  itemSelector: '.box',
		  // stamp elements
			stamp: '.stamp',
		  percentPosition: true,
		  masonry: {
		    // use element for option
		    columnWidth: '.box-sizer'
		  }
		});

		// layout Isotope after each image loads
		$grid.imagesLoaded().progress( function() {
		  $grid.isotope('layout');
		});
	});
})(window, document, undefined);