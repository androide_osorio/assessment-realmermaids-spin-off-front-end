# Maaji RealMermaids spin-off front-end

This is the front-end implementation of maaji #realmermaids for testing uses ONLY inside OWAK, for new job candidates to the back-end developer vacancies.

# Dependencies

* SASS and compass Gems
* Zurb's Foundation 5 Framework
* fontawesome
* jQuery Isotope
* jQuery imagesloaded

# Installation

Clone this repository in the desired location in your server or local machine. Run in your terminal:

```
#!
$ bower install
$ bundle
```
The commands will install all the dependencies and node modules used to generate the source files for styling and javascript. 